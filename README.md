# Ybook
## Description
```
Ybook is a social newtwork website and pwa aimed for ynov students.
Authentication is managed by AWS Cognito.
The database used is Prisma.
Built with Nest framework.
```

## Getting started

Clone the git repo with https or ssh.
### Installation

```bash
$ npm install
```
### Setup
```
add the .env file in the backend folder
```
## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Swagger

Swagger is available at this [url](http://localhost:3000/api)

## Authors
    Karam Florent
    Augier Nicolas