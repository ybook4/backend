FROM node:16.12-alpine

WORKDIR /usr/src/app

# build dependencies
COPY package*.json ./

RUN npm i

COPY . .

RUN npx prisma generate

RUN npm run build
