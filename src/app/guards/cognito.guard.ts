/* eslint-disable prettier/prettier */
import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { CognitoJwtVerifier } from 'aws-jwt-verify';
import { PrismaClient } from '@prisma/client';

//To use this guard add @UseGuards(AccessTokenGuard, AdminGuard) to your controller
@Injectable()
export class CognitoGuard implements CanActivate {
    verifier = CognitoJwtVerifier.create({
        userPoolId: process.env.USER_POOL_ID,
        tokenUse: "id",
        clientId: process.env.CLIENT_ID,
    });

    async canActivate(context: ExecutionContext): Promise<boolean> {
        try {
            const request = context.switchToHttp().getRequest();
            const decodedToken = await this.checkToken(request.headers.authorization.split(' ')[1]);
            
            let prisma = new PrismaClient();
            let user = await prisma.user.findUnique({
                where: {
                    email: decodedToken.email.toString()
                }
            });
            request.user = decodedToken;
            request.user.id = user.id;
            return true
        } catch (error) {
            console.log(error);
            
            return false
        }
    }
    //
    async checkToken(token: string) {
        return this.verifier.verify(
            token // the JWT as string
        )
    }
    //
}