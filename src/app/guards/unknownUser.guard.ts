/* eslint-disable prettier/prettier */
import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { CognitoJwtVerifier } from 'aws-jwt-verify';

//To use this guard add @UseGuards(AccessTokenGuard, AdminGuard) to your controller
@Injectable()
export class UnknownUserGuard implements CanActivate {
    verifier = CognitoJwtVerifier.create({
        userPoolId: process.env.USER_POOL_ID,
        tokenUse: "id",
        clientId: process.env.CLIENT_ID,
    });

    async canActivate(context: ExecutionContext): Promise<boolean> {
        try {
            const request = context.switchToHttp().getRequest();
            const decodedToken = await this.checkToken(request.headers.authorization.split(' ')[1]);
            request.token = decodedToken;
            return true
        } catch (error) {
            return false
        }
    }
    //
    async checkToken(token: string) {
        return this.verifier.verify(
            token // the JWT as string
        )
    }
    //
}