import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request, HttpException, HttpStatus } from '@nestjs/common';
import { CognitoGuard } from '../guards/cognito.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { CreatePostDto } from './dto/create-post.dto';
import { PostService } from './post.service';

@ApiTags('post')
@Controller('post')
export class PostController {
  constructor(private readonly postsService: PostService) { }
  //
  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Get(":numberOfPost/:pageNumber")
  findAll(@Param('numberOfPost') numberOfPost: number, @Param('pageNumber') pageNumber: number) {
    return this.postsService.findAll(numberOfPost, pageNumber);
  }
  //
  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Post(':userId')
  create(@Param('userId') id: number, @Body() createUserDto: CreatePostDto, @Request() req) {
    if (id !== req.user.id) {
      throw new HttpException('Id of token and param should be equal', HttpStatus.UNAUTHORIZED);
    }
    return this.postsService.create(id, createUserDto);
  }

  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Post(':postId/:userId/like')
  likePost(@Param('postId') postId: number, @Param('userId') userId: number, @Request() req) {
    if (userId !== req.user.id) {
      throw new HttpException('Id of token and param should be equal', HttpStatus.UNAUTHORIZED);
    }
    return this.postsService.likePost(userId, postId);
  }
  //
  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Post(':postId/:userId/unlike')
  unlikePost(@Param('userId') userId: number, @Param('postId') postId: number, @Request() req) {
    if (userId !== req.user.id) {
      throw new HttpException('Id of token and param should be equal', HttpStatus.UNAUTHORIZED);
    }
    return this.postsService.unlikePost(userId, postId);
  }
  //
  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Post(':postId/:userId/comment')
  commentPost(@Param('userId') userId: number, @Param('postId') postId: number, @Body() comment, @Request() req) {
    if (userId !== req.user.id) {
      throw new HttpException('Id of token and param should be equal', HttpStatus.UNAUTHORIZED);
    }
    return this.postsService.commentPost(userId, postId, comment.text);
  }

  //
  // @UseGuards(CognitoGuard)
  // @ApiBearerAuth('acces-token')
  // @Delete(':id')
  // remove(@Param('id') id: number) {
  //   return this.postsService.remove(id);
  // }
}
