import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PrismaClient, User } from '@prisma/client';
import { CreatePostDto } from './dto/create-post.dto';

@Injectable()
export class PostService {

    create(userId: number, createPostDto: CreatePostDto) {
        const prisma = new PrismaClient();
        return prisma.post.create({
            data: {
                htmlContent: createPostDto.htmlContent,
                userId: userId,
            }
        });
    }
    //
    async findAll(take: number, skip: number) {
        const prisma = new PrismaClient();
        let posts = await prisma.post.findMany({
            include: {
                postLikes: true,
                postComments: true,
                postAttachments: true,
                user: true,
            }, 
            orderBy:{
                createdAt: 'desc'
            }, 
            take: take, 
            skip: skip
        });
        let arrayToReturn = [];
        for (let i = posts.length; i > 0; i--) {
            arrayToReturn.push(posts[i - 1]);
        }
        return arrayToReturn;
    }

    remove(postId: number) {
        const prisma = new PrismaClient();
        return prisma.post.delete({
            where: { id: postId },
        })
    }
    //
    async likePost(userId: number, postId: number) {
        const prisma = new PrismaClient();
        const post = await prisma.post.findFirst(
            {
                where: { id: postId, postLikes: { none: { userId } } }
            }
        )
        if (!post) {
            throw new UnauthorizedException('User already liked this post');
        }
        return prisma.post.update(
            {
                where: { id: postId },
                data: {
                    postLikes: {
                        create: { userId: userId }
                    }
                }
            })
    }
    //
    async unlikePost(userId: number, postId: number) {
        const prisma = new PrismaClient();
        const post = await prisma.post.findFirst(
            {
                where: { id: postId, postLikes: { some: { userId } } }
            }
        );
        if (!post) {
            throw new UnauthorizedException('User did not like this post');
        }
        return prisma.post.update(
            {
                where: { id: postId },
                data: {
                    postLikes: {
                        deleteMany: { userId }
                    }
                }
            })
    }
    //
    async commentPost(userId: number, postId: number, comment: string) {
        console.log(comment);

        const prisma = new PrismaClient();
        return prisma.post.update(
            {
                where: { id: postId },
                data: {
                    postComments: {
                        create: {
                            userId: userId,
                            text: comment,
                        }
                    }
                }
            })
    }
}
