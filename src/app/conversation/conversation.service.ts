import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { NotFoundError } from 'rxjs';
import { CreateConversationDto } from './dto/create-conversation.dto';
import { SendMessageDto } from './dto/send-message.dto';

@Injectable()
export class ConversationService {
  prisma = new PrismaClient();
  constructor() {}
  //
  async get(userId: number, conversationId: number) {
    const conversation = await this.prisma.conversation.findUnique({
      where: {
        id: conversationId,
      },
      include: {
        messages: true,
      },
    });
    if (!conversation) {
      throw new NotFoundException("Conversation doesn't exist");
    }
    if (conversation.fromId != userId && conversation.toId != userId) {
      throw new NotFoundException(
        "You don't have permission to get this conversation",
      );
    }
    return conversation;
  }

  async getAll(userId: number) {
    const conversations = await this.prisma.conversation.findMany({
      where: {
        OR: [{ fromId: userId }, { toId: userId }],
      },
    });
    return conversations;
  }
  //
  async create(conversationDto: CreateConversationDto) {
    const check = await this.prisma.conversation.findFirst({
      where: {
        fromId: { in: [conversationDto.fromId, conversationDto.toId] },
        toId: { in: [conversationDto.fromId, conversationDto.toId] },
      },
    });
    if (check) {
      return check;
    }
    const conversation = await this.prisma.conversation
      .create({
        data: {
          fromId: conversationDto.fromId,
          toId: conversationDto.toId,
        },
      })
      .catch((error) => {
        throw new NotFoundException("User doesn't exist");
      });
    return this.getConversationMessages(
      conversationDto.content,
      conversationDto.fromId,
      conversation.id,
    );
  }

  async send(userId: number, sendMessageDto: SendMessageDto) {
    const conversation = await this.prisma.conversation.findFirst({
      where: {
        id: sendMessageDto.conversationId,
      },
    });
    if (!conversation) {
      throw new NotFoundException("Conversation doesn't exist");
    }
    return this.getConversationMessages(
      sendMessageDto.content,
      userId,
      sendMessageDto.conversationId,
    );
  }
  async getConversationMessages(
    content: string,
    userId: number,
    conversationId: number,
  ) {
    return this.prisma.conversationMessage.create({
      data: {
        content: content,
        userId: userId,
        conversationId: conversationId,
      },
    });
  }
}
