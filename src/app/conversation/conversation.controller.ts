import { Body, Controller, Get, Param, Post, UseGuards, Request, HttpException, HttpStatus } from '@nestjs/common';
import { ConversationService } from './conversation.service';
import { CreateConversationDto } from './dto/create-conversation.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SendMessageDto } from './dto/send-message.dto';
import { CognitoGuard } from '../guards/cognito.guard';

@ApiTags('conversation')
@Controller('conversation')
export class ConversationController {
    constructor(private readonly conversationService: ConversationService) { }
    //
    @UseGuards(CognitoGuard)
    @ApiBearerAuth('acces-token')
    @Get(':userId')
    findAll(@Param('userId') userId: number, @Request() req) {
        return this.conversationService.getAll(userId);
    }

    @UseGuards(CognitoGuard)
    @ApiBearerAuth('acces-token')
    @Get(':conversationId/:userId')
    get(@Param('conversationId') conversationId: number, @Param('userId') userId: number, @Request() req) {
        return this.conversationService.get(userId, conversationId);
    }
    //

    @UseGuards(CognitoGuard)
    @ApiBearerAuth('acces-token')
    @Post()
    create(@Body() conversationDto: CreateConversationDto) {
        return this.conversationService.create(conversationDto);
    }

    @UseGuards(CognitoGuard)
    @ApiBearerAuth('acces-token')
    @Post('message/:userId')
    createMessage(@Param('id') userId: number, @Body() sendMessageDto: SendMessageDto, @Request() req) {
        return this.conversationService.send(userId, sendMessageDto);
    }
}
