import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsNumber } from "class-validator";


export class CreateConversationDto {

    @ApiProperty()
    @IsNumber()
    fromId: number;

    @ApiProperty()
    @IsNumber()
    toId: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    content: string

}