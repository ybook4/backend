import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsNumber } from 'class-validator';

export class SendMessageDto {
  @ApiProperty()
  @IsNumber()
  conversationId: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  content: string;
}
