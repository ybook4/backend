import { Injectable } from '@nestjs/common';
import { PrismaClient, User } from '@prisma/client';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {

  create(createUserDto: CreateUserDto) {
    const prisma = new PrismaClient();
    return prisma.user.create({
      data: {
        email: createUserDto.email,
        firstname: createUserDto.firstname,
        lastname: createUserDto.lastname,
      }
    });
  }
  //
  findAll() {
    const prisma = new PrismaClient();
    return prisma.user.findMany();
  }
  //
  findOneByEmail(email: string) {
    //add token<=>id match
    const prisma = new PrismaClient();
    return prisma.user.findUnique({
      where: { email },
      include: {
        posts: true,
        postLikes: true,
        postComment: true,
        fromFriendship: true,
        toFrienship: true,
        blockedUsers: true,
        blockedByUsers: true,
        conversationsSent: true,
        conversationsReceived: true,
        conversationMessages: true
      }
    });
  }

  async updateUser(email: string, updateUserDto: CreateUserDto) {
    const prisma = new PrismaClient();
    try {
      return await prisma.user.update({
        where: { email },
        data: {
          email: updateUserDto.email,
          firstname: updateUserDto.firstname,
          lastname: updateUserDto.lastname,
        }
      });
    }
    catch (error) {
      return error;
    }
  }

  remove(email: string) {
    //add token<=>id match
    const prisma = new PrismaClient();
    return prisma.user.delete({
      where: { email }
    });
  }
  //
}
