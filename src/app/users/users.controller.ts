import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { CognitoGuard } from '../guards/cognito.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UnknownUserGuard } from '../guards/unknownUser.guard';

@ApiTags('user')
@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @UseGuards(UnknownUserGuard)
  @ApiBearerAuth('acces-token')
  @Post('/signup')
  create(@Body() createUserDto: CreateUserDto, @Request() req) {
    return this.usersService.create(createUserDto);
  }
  // 
  @Get()
  findAll() {
    return this.usersService.findAll();
  }
  //
  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Get(':email')
  findOneByEmail(@Param('email') email: string, @Request() req) {
    this.checkEmail(email, req.user.email);
    return this.usersService.findOneByEmail(email);
  }

  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Patch(':email')
  update(@Param('email') email: string, @Body() updateUserDto: CreateUserDto, @Request() req) {
    this.checkEmail(email, req.user.email);
    return this.usersService.updateUser(email, updateUserDto);
  }
  //
  @UseGuards(CognitoGuard)
  @ApiBearerAuth('acces-token')
  @Delete(':email')
  remove(@Param('email') email: string, @Request() req) {
    this.checkEmail(email, req.user.email);
    return this.usersService.remove(email);
  }
  //
  checkEmail(email: string, tokenEmail: string) {
    if (email !== tokenEmail) {
      throw new HttpException('Email of token and param should be equal', HttpStatus.UNAUTHORIZED);
    }
  }
}

