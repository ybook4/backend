import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
  .setTitle('Ybook')
  .setDescription('The ybook API description')
  .addBearerAuth({
    type : 'http', scheme: 'bearer', bearerFormat: 'JWT'},'acces-token'
  )
  .setVersion('1.0')
  .build();
  app.enableCors();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  
  app.useGlobalPipes(new ValidationPipe(
    {
      whitelist: true,
      skipUndefinedProperties: true,
      transform: true,
    }
  ));
  
  await app.listen(3000);
  console.log(`Swagger is running on: http://localhost:3000/api`);
  
}
bootstrap();
