import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './app/users/users.module';
import { PostModule } from './app/posts/post.module';
import { ConversationModule } from './app/conversation/conversation.module';

@Module({
  imports: [
    UsersModule,
    PostModule,
    ConversationModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
