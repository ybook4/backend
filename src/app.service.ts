import { Injectable } from '@nestjs/common';
import { PrismaClient, User } from '@prisma/client';

@Injectable()
export class AppService {
  constructor() {

  }
  async getUsers() {
    const prisma = new PrismaClient();
    const users = prisma.user.findMany();
  }
}
